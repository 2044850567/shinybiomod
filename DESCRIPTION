Package: ShinyBIOMOD
Title: A new R application for Modelling Species Distribution
Version: 0.0.0.9000
Authors@R: person("Ian", "Ondo", email = "iondo@kew.org", role = c("aut", "cre"), comment = c(ORCID = "0000-0001-7816-5882"))
Description: The tool combines the powerful statistical modeling framework developed within
                biomod2, with an appealing easy-to-use interface designed by shiny in order to investigate
                the drivers of species distributions and project them into space and time.
                shinyBIOMOD incorporates new options to account for sampling bias in observation data; selecting variables while limiting (multi-)collinearity issues among predictors;
                visualizing and exporting the results...etc.
                It facilitates the modelling process for both experts and beginners in programming and statistics.
Depends: R (>= 3.4.3)
License: use_gpl3_license()
Encoding: UTF-8
LazyData: true
RoxygenNote: 7.1.1
StagedInstall: no
Imports: 
    gtools (>= 3.9.2),
    ggplot2 (>= 3.3.4),
    shiny (>= 1.6.0),
    biomod2,
    shinyBS (>= 0.61),
    shinyjs (>= 2.0.0),
    shinysky (>= 0.1.3),
    shinyWidgets (>= 0.6.0),
    leaflet (>= 2.0.4.1),
    leaflet.extras (>= 1.0.0),
    raster (>= 3.4-13),
    rasterVis (>= 0.50.2),
    ade4 (>= 1.7-17),
    elasticnet (>= 1.3),
    matrixStats,
    mapview,
    dismo (>= 1.3-3),
    DT (>= 0.18),
    bestglm (>= 0.37.3),
    splancs,
    spatstat (>= 2.1-0),
    data.table (>= 1.14.0),
    stringr (>= 1.4.0),
    spThin (>= 0.2.0),
    lwgeom (>= 0.2-6),
    sf (>= 1.0-0),
    rgdal (>= 1.5.23),
    rgeos (>= 0.5-5),
    geosphere (>= 1.5-10),
    RGeodata,
    lattice,
    RColorBrewer (>= 1.1-2),
    parallel,
    foreach,
    doParallel,
    usdm (>= 1.1-18),
    PresenceAbsence (>= 1.1.9),
    webshot,
    shinydashboard (>= 0.7.1),
    htmlTable (>= 2.2.1),
    magrittr (>= 2.0.1)
Remotes: andrewsali/shinycssloaders, gitlab::IanOndo/shbiomod2, github::IanOndo/RGeodata, AnalytixWare/ShinySky
