ui_workflow <- fluidPage(

			titlePanel(h1(strong(em("Workflow")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1; color: black")),

			br(),

			fluidRow(

				column(12,
					div(style="text-align: justify; font-family: 'times'; font-size: 18px; width: 60%;",
						p("The scheme below represents the",strong("general workflow"),"of ShinyBIOMOD. It describes how the user is guided throughout a series of mandatory and non-mandatory steps to build a",strong("Species Distribution Model (SDM)"),"with the possibility of exploring and visualizing data at every of these steps.
						The workflow is designed to help the user to optimize the modelling process for best results and high efficiency.",br(),"ShinyBIOMOD is an interactive tool that considers the data available to the user at each step before proceeding to the next ones.
						  Indeed, new tabs and options appear dynamically according to the set of information entered by the user as he advances along the modelling process.")
					)
				)
			),

			fluidRow(

				column(8,

					br(),br(),

					tags$head(
						tags$style(".shiny-image-output width: 95%; height: 80%; text-align: center;")
					),
					tags$img(src='ShinyBIOMOD_workflow_chartv2.png', height='80%', width='95%')

				),

				br(),

				column(4,

					br(),

					div(style="font-family: 'times'; font-size: 18px; width: 95%; height: 80%;",
						p("Here is how it works:",br(),
						strong("0. Set directories:"),
							tags$ul(
								tags$li("Select a working directory to save the results of your modelling session (you can access outputs saved by browsing the application architecture described in tab 'structure' above)."),
								tags$li("Optionally, specify the path to your ArcGIS folder to generate maps with ArcGIS")
							),
						strong("1. Data upload:"),
							tags$ul(
								tags$li("Upload your occurrence and environmental data in one dataset or alternatively in separate datasets"),
								tags$li("Map the geographic distribution of your occurrence points ")
							),
						strong("2. Data processing:"),
							tags$ul(
								tags$li("Filter your occurrence points with the Spatial Thinning tool"),
								tags$li("Define a geographic area to spatially constrain the calibration of your model(s)")
							),
						strong("3. Data exploration:"),
							tags$ul(
								tags$li("Explore correlation and variation among your environmnental variables by using Principal Component Analysis (PCA) and sparse PCA tools"),
								tags$li("Explore (multi-)collinearity issues with Pairwise-correlations and Variance Inflation Factor (VIF) metrics")
							),
						strong("4. Data preparation:"),
							tags$ul(
								tags$li("Extract environmental data at your occurrence locations")
							),
						strong("5. ENM:"),
							tags$ul(
								tags$li("Format your dataset with the Biomod Formatting tool"),
								tags$li("Model the distribution of your species with the Biomod Modelling tool"),
								tags$li("Combine several models together to build an ensemble model with the Biomod Ensemble Modelling tool")
							),
						strong("6. Evaluation:"),
							tags$ul(
								tags$li("Assess the performance of your model(s) by inspecting evaluation metrics"),
								tags$li("Assess the importance of your environmental predictors"),
								tags$li("Assess the response of your species across environmental gradients by plotting the",em('species-environment'),"response curves")
							),
						strong("7. Projection:"),
							tags$ul(
								tags$li("Project your ensemble model(s) into current or new environmental conditions with the Biomod Ensemble Forecasting tool"),
								tags$li("Convert suitability (continuous) map(s) into species range (binary) map(s) using different criteria"),
								tags$li("Optionally use ArcGIS to generate maps and map template(s) with a legend summarizing your species distribution model(s)")
							),
						strong("8. Report:"),
							tags$ul(
								tags$li("Select the modelling steps that you want to report and generate a workflow session report or a script code summarizing your activites")
							)
						),
						div(style='font-size: 13px',p(em("Note: In the future steps from 2 to 4 can be skipped if the user provides a dataset with environmental variables already extracted at the occurrence locations or if the user has selected a working directory with a previously saved shinyBIOMOD project."),br(),
						em("In the latter case the application will detect the objects saved from the previous session and will propose appropriate sections.")))
					)
				)
			)
)
