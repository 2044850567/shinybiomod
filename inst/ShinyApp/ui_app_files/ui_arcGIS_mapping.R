ui_arcgismapping <- fluidPage(

					useShinyjs(),

					extendShinyjs("www/js/app-shinyjs.js", functions = c("getInputType")),


					titlePanel(h1(strong(em('ArcGIS mapping')), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;")),

					sidebarLayout(position = "left",

						sidebarPanel(

							tags$head(

								tags$script(src = 'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML', type = 'text/javascript',includeScript("style/google-analytics.js"))

							),

							br(),

							uiOutput("argis_mapping_control_panel")

						),

						mainPanel(

							uiOutput("argis_mapping_output_map")
						)
					)
				)
