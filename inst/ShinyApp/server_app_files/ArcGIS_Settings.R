# --------------------------------------------------------

# TRIGGERS

# --------------------------------------------------------
hasArcGISPythonExe <- reactiveVal(FALSE)

# searches for ArcGIS python executable
observeEvent(input$search_arcGIS_python_path, ignoreNULL=TRUE,{

	handlerExpr = {
		# condition prevents handler execution on initial app launch
		if (input$search_arcGIS_python_path > 0) {
			shinyjs::show('arcgis_settings_busy_indicator')
			env.var.path <- unlist(str_split(Sys.getenv("PATH"),";"))#unlist(str_split(shell("echo %PATH%", intern=TRUE),";"))
			best_guess  <- grep("[Pp][Yy][Tt][Hh][Oo][Nn][[:digit:]]{2}", env.var.path)
			path <- NULL
			if(length(best_guess)>0L){
			  path <- grep("ArcGIS[0-9]+[.][1-9]/python.exe", list.files(env.var.path[best_guess], ".exe$", full.names=TRUE, recursive=TRUE), value=TRUE)
			}
			if(length(path)==0) {
  			list.python.path  	<- sapply(env.var.path, function(x) grep("ArcGIS[0-9]+[.][1-9]/python.exe", list.files(x, ".exe$", full.names=TRUE, recursive=TRUE), value=TRUE))
  			path 				<- if(length(list.python.path[lengths(list.python.path)>0L])>0L) dirname(list.python.path[lengths(list.python.path)>0L][[1]]) else NULL
			}
  		if(!is.null(path)){ # update the widget value
  		  path <- gsub("\\\\","/",path)
  		  # save the path in an environment variable
  		  Sys.setenv(SHINY_BIOMOD_ARCGIS_DIR=dirname(path))
				updateDirectoryInput(session, 'arcgis_mapping_python_path_directory', value = dirname(path))
  		  hasArcGISPythonExe(TRUE)
			}else{
				shinyjs::show('arcgis_settings_search_disclaimer')
				shinyjs::hide('arcgis_settings_busy_indicator')
			}
		}
		shinyjs::hide('arcgis_settings_busy_indicator')
	}

})

# updates the directory of arcgis python executable
observeEvent(
	ignoreNULL = TRUE,

	eventExpr = {
		input$arcgis_mapping_python_path_directory
	},
	handlerExpr = {
		# condition prevents handler execution on initial app launch
		if (input$arcgis_mapping_python_path_directory > 0) {
			# launch the directory selection dialog with initial path read from the widget
			path = choose.dir(default = readDirectoryInput(session, 'arcgis_mapping_python_path_directory'))

			if(file.exists(file.path(path,'python.exe'))){
				# update the widget value
				updateDirectoryInput(session, 'arcgis_mapping_python_path_directory', value = path)
			  hasArcGISPythonExe(TRUE)
				# hide the disclaimers
				shinyjs::hide('arcgis_settings_search_disclaimer')
				shinyjs::hide('arcgis_settings_directory_disclaimer')
			}
			else{
				shinyjs::show('arcgis_settings_directory_disclaimer')
			}
		}
	}
)

observeEvent(hasArcGISPythonExe(),ignoreNULL=TRUE, {
  if(hasArcGISPythonExe()){
    shinyjs::hide('ArcGIS_settings_info_tab_panel')
    showTab(inputId="navb",
            target = "Mapping with ArcGIS"
    )
    tabs$names = c(tabs$names,"Mapping with ArcGIS")
  }else{
    hideTab(inputId="navb",
            target = "Mapping with ArcGIS"
    )
  }
})

