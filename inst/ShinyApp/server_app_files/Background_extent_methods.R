background_extent_analysis_selected_extent <- reactiveValues()
# --------------------------------------------------------

# TRIGGERS

# --------------------------------------------------------
is_plotted_background_extent <- reactiveVal(FALSE)
has_computed_background_extent <- reactiveVal(FALSE)
can_save_background   <- reactiveVal(FALSE)
can_delete_background <- reactiveVal(FALSE)
hasSavedBackground <- reactiveVal(FALSE)

current_background <- reactiveVal(1)

# --------------------------------------------------------

# OBSERVERS

# --------------------------------------------------------

observe({

  toggleState(id="save_background_extent_Clip", condition=can_save_background())

})

observe({

  toggleState(id="delete_background_extent_Clip", condition=can_delete_background())

})


# show/hide navigation arrows in the box info
observe({

  if(!is.null(input$background_area_sp$right) && length(input$background_area_sp$right)==0L){
    shinyjs::hide("background_extent_box_move_left")
    shinyjs::hide("background_extent_box_move_right")
  }else{
    list.background <- list.files(dirname(rasterTmpFile()), pattern=paste0("^myPolygon_.+\\.rds$"), full.names=TRUE)
    list.species.background <- unlist(sapply(gsub("\\(([^()]+)\\)","(.*?)",input$background_area_sp$right), grep, x=list.background,value=TRUE,USE.NAMES=FALSE))
    toggleState(id="background_extent_box_move_left", condition=current_background()==1)
    toggleState(id="background_extent_box_move_right", condition=current_background()==length(list.species.background))
  }

})


# show/hide background extent options with occurrence data
observe({
	if("Data processing" %in% tabs$names && (!is.null(data_input()) | !is.null(data_select())) ){

	  shinyjs::hide(id="background_extent_tooltip_info_occ_data")

	  #"Draw a constrained area from the map (Not implemented yet)" = 'TOOL',
	  # "Use an Environmental Profiling (Not implemented yet)" = 'ENVPROF',
	  updateRadioButtons(session, 'background_extent', choices=c("Use a Pre-defined region" = 'POLY',"Use a Bounding Box"='BBOX', "Use a Convex Hull" = 'HULL', "Use Circular Buffers"='BUFFER'), selected=isolate(input$background_extent))

		available_occ_data <- NULL
		if(!is.null(data_input()))
			available_occ_data <- c(available_occ_data, "Your raw occurence data" = "raw_occ")

		if(!is.null(data_select()))
			available_occ_data <- c("Your subsetted occurence data" = "subset_occ", available_occ_data)

		if(sum(lengths(reactiveValuesToList(spatial_thinning_analysis_selected_dataset)))>0L){
		  if(sum(lengths(reactiveValuesToList(spatial_thinning_analysis_selected_dataset)))>0L)
		    available_occ_data <- c("Your spatially filtered occurence data" = "filter_occ", available_occ_data)
    }
		updateSelectizeInput(session, 'occ_data_for_background_extent', choices=available_occ_data)
	}

})

# show/hide background extent panel to select occurrence data
observeEvent(input$background_extent,ignoreNULL=TRUE,{
	if(input$background_extent %in% c('ENVPROF','BBOX','HULL','BUFFER')){

	  # hide pre-defined region panel
	  shinyjs::hide('background_extent_polygon_panel')

	  # show occurrence related panel
		shinyjs::show('background_extent_occ_data_panel')
    shinyjs::show('background_extent_add_occ_data_div')

    if(input$background_extent %in% c('BUFFER','HULL','BBOX'))
      shinyjs::show('background_extent_buffer_panel')
    else
      shinyjs::hide('background_extent_buffer_panel')
	}
	else{
		shinyjs::hide('background_extent_occ_data_panel')

	  if(input$background_extent=='POLY')
	    shinyjs::show('background_extent_polygon_panel')
	  else
	    shinyjs::hide('background_extent_polygon_panel')
	}
})

# show/hide polygon info tab
observe({
	if(is_plotted_background_extent()){
		shinyjs::show('Background_extent_Polygon_Info')
	}
	else{
		shinyjs::hide('Background_extent_Polygon_Info')
	}
})

# updates name of the focal species (and info displayed) when the list available of species changes
observeEvent(input$background_area_sp$right, ignoreNULL=TRUE,{

  if(length(input$background_area_sp$right)>0){

    list.background <- list.files(dirname(rasterTmpFile()), pattern=paste0("^myPolygon_.+\\.rds$"), full.names=TRUE)
    list.species.background <- unlist(sapply(gsub("\\(([^()]+)\\)","(.*?)",input$background_area_sp$right), grep, x=list.background,value=TRUE,USE.NAMES=FALSE))

    if(length(list.species.background)==0L) return()

    # update current index
    index.value <- 1 #min(current_background(), length(list.species.background))
    current_background(index.value)

    # update species focal
    species_names = str_match(basename(list.species.background), pattern=paste0("(?<=hull|bbx|shp|circle)(.*?)", paste(colnames(data_occ_background()),collapse='')))[,2]
    speciesOcc$focal <- species_names[current_background()]
  }

})

# updates name of the focal species (and info displayed) whilst the user press move right/left button
observeEvent(input$background_extent_box_move_right, ignoreNULL=TRUE,{

  if(input$background_extent_box_move_right>0){

    list.background <- list.files(dirname(rasterTmpFile()), pattern=paste0("^myPolygon_.+\\.rds$"), full.names=TRUE)
    list.species.background <- unlist(sapply(gsub("\\(([^()]+)\\)","(.*?)",input$background_area_sp$right), grep, x=list.background,value=TRUE,USE.NAMES=FALSE))

    # update current index
    index.value <- min(current_background()+1, length(list.species.background))
    current_background(index.value)

    # update species focal
    species_names = str_match(basename(list.species.background), pattern=paste0("(?<=hull|bbx|shp|circle)(.*?)", paste(colnames(data_occ_background()),collapse='')))[,2]
    speciesOcc$focal <- species_names[current_background()]
  }

})

observeEvent(input$background_extent_box_move_left, ignoreNULL=TRUE,{

  if(input$background_extent_box_move_left>0){

    list.background <- list.files(dirname(rasterTmpFile()), pattern=paste0("^myPolygon_.+\\.rds$"), full.names=TRUE)
    list.species.background <- unlist(sapply(gsub("\\(([^()]+)\\)","(.*?)",input$background_area_sp$right), grep, x=list.background,value=TRUE,USE.NAMES=FALSE))

    # update current index
    index.value <- max(current_background()-1, 1)
    current_background(index.value)

    # update species focal
    species_names = str_match(basename(list.species.background), pattern=paste0("(?<=hull|bbx|shp|circle)(.*?)", paste(colnames(data_occ_background()),collapse='')))[,2]
    speciesOcc$focal <- species_names[current_background()]
  }

})

# save background extent shape file
observeEvent(

	ignoreNULL = TRUE,

	eventExpr={
		input$save_background_extent_Clip
	},

	handlerExpr = {

		withBusyIndicatorServer(buttonId="save_background_extent_Clip",

			expr = {

			  list.background <- list.files(dirname(rasterTmpFile()), pattern=paste0("^myPolygon_.+\\.rds$"), full.names=TRUE)
			  list.species.background <- unlist(sapply(gsub("\\(([^()]+)\\)","(.*?)",input$background_area_sp$right), grep, x=list.background,value=TRUE,USE.NAMES=FALSE))


			  species_names = str_match(basename(list.species.background), pattern=paste0("(?<=hull|bbx|shp|circle)(.*?)", paste(colnames(data_occ_background()),collapse='')))[,2]
			  species_name <- species_names[current_background()]

			  species.background.names = str_match(basename(list.species.background), pattern=paste0("^myPolygon",'.(.*?)', species_name))[,2]
			  species.background.name <- species.background.names[current_background()]

			  species.background.file = list.species.background[current_background()]


				Sys.sleep(1)
				if(file.exists(species.background.file)){

				  # convert reactiveValues object to list
				  list_background_extent <- isolate(reactiveValuesToList(background_extent_analysis_selected_extent))
				  # create empty list
				  l <- list()
				  # add background
				  l[[species.background.name]] <- readRDS(species.background.file)

				  if(length(list_background_extent[[species_name]])==0L){
				    # create new background list
  				  background_extent_analysis_selected_extent[[species_name]] <- l
				  }else{
				    # append new background to existing list of backgrounds
				    background_extent_analysis_selected_extent[[species_name]] <- append(background_extent_analysis_selected_extent[[species_name]],l)
				  }

					rasters$background <- input$background_extent
					can_save_background(FALSE)
          can_delete_background(TRUE)
          hasSavedBackground(TRUE)

				}
			}
		)
	}
)

# delete background extent shape file
observeEvent(

  ignoreNULL = TRUE,

  eventExpr={
    input$delete_background_extent_Clip
  },

  handlerExpr = {

    withBusyIndicatorServer(buttonId="delete_background_extent_Clip",

            expr = {

              list.background <- list.files(dirname(rasterTmpFile()), pattern=paste0("^myPolygon_.+\\.rds$"), full.names=TRUE)
              list.species.background <- unlist(sapply(gsub("\\(([^()]+)\\)","(.*?)",input$background_area_sp$right), grep, x=list.background,value=TRUE,USE.NAMES=FALSE))

              species_names = str_match(basename(list.species.background), pattern=paste0("(?<=hull|bbx|shp|circle)(.*?)", paste(colnames(data_occ_background()),collapse='')))[,2]
              species_name <- species_names[current_background()]

              species.background.names = str_match(basename(list.species.background), pattern=paste0("^myPolygon",'.(.*?)', species_name))[,2]
              species.background.name <- species.background.names[current_background()]

              Sys.sleep(1)
              if (!is.null(reactiveValuesToList(background_extent_analysis_selected_extent)[[species_name]][[species.background.name]])){

                # convert reactiveValues object to list
                list_background_extent <- isolate(reactiveValuesToList(background_extent_analysis_selected_extent))

                # remove background of the species from the list
                list_background_extent[[species_name]][[species.background.name]] <- NULL

                # reconstruct from updated list
                background_extent_analysis_selected_extent <- do.call(reactiveValues,list_background_extent)

                # check removal
                if(is.null(isolate(reactiveValuesToList(background_extent_analysis_selected_extent))[[species_name]][[species.background.name]])){
                  rasters$background <- NULL
                  can_delete_background(FALSE)
                  can_save_background(TRUE)
                }

                if(sum(lengths(list_background_extent))==0L)
                  hasSavedBackground(FALSE)

              }
         }
    )
  }
)

# Run the clipping of the training area
observeEvent(

  eventExpr={

    input$background_extent_Clip

  },

  ignoreNULL=TRUE,

  handlerExpr={

    withBusyIndicatorServer(buttonId="background_extent_Clip", {

      if(input$background_extent_Clip>0){

        updateRadioButtons(session, 'background_extent', selected=input$background_extent)

        if(!is.null(input$background_area_sp$right) && length(input$background_area_sp$right)>0L){
          myBackground()
          sp.to.run = setdiff(input$background_area_sp$right, speciesOcc$focal)
          while(length(sp.to.run) > 0L){
            speciesOcc$focal <- sp.to.run[1]
            myBackground()
            sp.to.run = setdiff(sp.to.run, speciesOcc$focal)
            Sys.sleep(1)
          }
          has_computed_background_extent(TRUE)
        }
      }

    })
  }
)

observeEvent(has_computed_background_extent(),ignoreNULL=TRUE, {

  if(has_computed_background_extent()){
    list.background <- list.files(dirname(rasterTmpFile()), pattern=paste0("^myPolygon_.+\\.rds$"), full.names=TRUE)
    list.species.background <- unlist(sapply(gsub("\\(([^()]+)\\)","(.*?)",input$background_area_sp$right), grep, x=list.background,value=TRUE,USE.NAMES=FALSE))
    species_names = str_match(basename(list.species.background), pattern=paste0("(?<=hull|bbx|shp|circle)(.*?)", paste(colnames(data_occ_background()),collapse='')))[,2]
    # display the most recent map
    current.index <- which.max(file.mtime(list.background))
    current_background(current.index)
    speciesOcc$focal  <- species_names[current_background()]
  }

})


# updates controls
observeEvent(input$background_extent, ignoreNULL=TRUE,{

  proxy <-leafletProxy("BgMap", data = data_occ_background())

  if(!is.null(data_occ_background()) && input$background_extent=='TOOL'){

    proxy <- proxy %>%

      addMeasure(primaryLengthUnit = "kilometers") %>%

      addDrawToolbar(targetGroup 	= "polyGons",

                     position	='topleft',

                     singleFeature = TRUE,

                     editOptions = editToolbarOptions(

                       edit = TRUE,

                       selectedPathOptions = selectedPathOptions(color="red", fillColor="red")

                     )
      )
  }else{
    proxy <- proxy %>% removeDrawToolbar(clearFeatures=TRUE)
  }

  #proxy

})

# updates the species map to be displayed
observeEvent(current_background(), ignoreNULL=TRUE, {

    if(!is.null(data_occ_background())){

      list.background <- list.files(dirname(rasterTmpFile()), pattern=paste0("^myPolygon_.+\\.rds$"), full.names=TRUE)
      list.species.background <- unlist(sapply(gsub("\\(([^()]+)\\)","(.*?)",input$background_area_sp$right), grep, x=list.background,value=TRUE,USE.NAMES=FALSE))
      species_names = str_match(basename(list.species.background), pattern=paste0("(?<=hull|bbx|shp|circle)(.*?)", paste(colnames(data_occ_background()),collapse='')))[,2]

      species.name  <- species_names[current_background()]
      my.background <- readRDS(list.species.background[current_background()])

      proxy       <- leafletProxy("BgMap", data = data_occ_background())

      if(input$background_extent_Clip >0L){

        if(!is.null(input$background_extent_add_occ_data) && input$background_extent_add_occ_data==TRUE){

          proxy <- proxy %>%

          addCircles(radius = 5000, layerId = paste0(data_occ_background()[,4],1:nrow(data_occ_background())), group="Occ",

                     lng = data_occ_background()[,2],#occ_dat_bg[,2],

                     lat = data_occ_background()[,3],#occ_dat_bg[,3],

                     popup = ~paste(ID),#occ_dat_bg[,1],

                     color = "black",

                     fillOpacity = 0.7
          )

          proxy <- proxy %>% removeShape(grep(species.name, paste0(data_occ_background()[,4],1:nrow(data_occ_background())),invert=TRUE,value=TRUE))

        }else{
          proxy <- proxy %>% clearGroup("Occ")
        }

      }

      if(!is.null(input$background_extent) && input$background_extent!='TOOL' && !is.null(myBackground())){

        proxy <- proxy %>%
          leaflet::clearGroup("Polygon")

        proxy <- proxy %>%

          leaflet::addPolygons(data = my.background, group="Polygon",

                      color="red",

                      fillColor="red",

                      fillOpacity = 0.3
          ) %>%

          setView(lat=median(sf::st_bbox(my.background)[c("ymin","ymax")]),lng=median(sf::st_bbox(my.background)[c("xmin","xmax")]), zoom=4, options=list(maxZoom=11))
      }

      #proxy
    }

})


# hide/display occurrence points
observe({

  input$background_extent
  input$background_extent_add_occ_data
  input$background_extent_Clip
  input$occ_data_for_background_extent

  if(is.null(input$background_extent_add_occ_data)) return()

  isolate({

    list.background <- list.files(dirname(rasterTmpFile()), pattern=paste0("^myPolygon_.+\\.rds$"), full.names=TRUE)
    list.species.background <- unlist(sapply(gsub("\\(([^()]+)\\)","(.*?)",input$background_area_sp$right), grep, x=list.background,value=TRUE,USE.NAMES=FALSE))
    species_names = str_match(basename(list.species.background), pattern=paste0("(?<=hull|bbx|shp|circle)(.*?)", paste(colnames(data_occ_background()),collapse='') ))[,2]

    species.name <- species_names[current_background()]

    if(!is.null(data_occ_background())){

      proxy       <-leafletProxy("BgMap", data = data_occ_background())

       if(input$background_extent_add_occ_data){

          proxy  <- proxy %>%

            addCircles(radius = 5000, layerId = paste0(data_occ_background()[,4],1:nrow(data_occ_background())),

                       lng = data_occ_background()[,2],

                       lat = data_occ_background()[,3],

                       popup = ~paste(ID),

                       color = "black",

                       fillOpacity = 0.7
            )
        proxy  <- proxy %>% removeShape(grep(species.name, paste0(data_occ_background()[,4],1:nrow(data_occ_background())),invert=TRUE,value=TRUE))

      }else{
        proxy  <- proxy %>% clearGroup("Occ")
      }

      #proxy
    }
  })

})

# updates polygon shapefile directory
observeEvent(

	ignoreNULL = TRUE,

	eventExpr = {
		input$backgroundshape_directory
	},

	handlerExpr = {
		# condition prevents handler execution on initial app launch
		if (input$backgroundshape_directory > 0) {

			# launch the directory selection dialog with initial path read from the widget
			path = choose.dir(default = readDirectoryInput(session, 'backgroundshape_directory'))
			path <- gsub("\\\\","/",path)

			# save the path in an environment variable
			Sys.setenv(SHINY_BIOMOD_BACKGROUND_DIR=path)
			# update the widget value
			updateDirectoryInput(session, 'backgroundshape_directory', value = path)
		}
	}

)


# updates list of layers available in region shapefile directory
observe({
	listLayers = backgroundShapelayers()
	isolate({
		if(!is.null(listLayers)){
			reordered = c(grep('SHBM',listLayers),grep('SHBM',listLayers,invert=TRUE))  # to select first the shapefile already processed by SHBM (if it exists!)
			updateSelectizeInput(session,inputId = "backgroundShape_files", choices = listLayers[reordered])
		}
	})
})

# updates choices of sub-geometries
observeEvent(input$checkBackgroundMap,{
	updateAwesomeCheckbox(session, "checkBackgroundMap", value = input$checkBackgroundMap)
})

observeEvent(input$Dissolve_BG, ignoreNULL=TRUE,{
	if(input$Run_dissolve >0)
		shinyjs::show("show_background_map_checkbox")
	else
		shinyjs::hide("show_background_map_checkbox")
})

# updates the polygon field name
observeEvent(input$backgroundFieldName,{
	if(!is.null(lengthBackground()) && lengthBackground()>1 && !is.null(input$backgroundFieldName))
		updateSelectizeInput(session,"backgroundFieldName",selected=input$backgroundFieldName)
})

# updates choices of sub-geometries
observe({
	if(!is.null(lengthBackground()) && lengthBackground()>1 && !is.null(input$backgroundFieldName)){
		if(!is.null(input$backgroundRegionName) && input$backgroundRegionName == 'all'){
			updateSelectizeInput(session,"backgroundRegionName",choices=c("All"='all'),selected=input$backgroundRegionName)
		}
	}

})

# dissolves and save the dissolved polygon when the user click on the run button
observeEvent(

	eventExpr={
		input$Dissolve_BG
	},

	handlerExpr = {

		withBusyIndicatorServer(buttonId="Dissolve_BG",

			expr = {

				background =  as(myBackground(),"Spatial")
				Sys.sleep(1)
				if (!is.null(background)){

					if (version_GEOS0() < "3.3.0") {

						if(!is.null(input$backgroundRegionName) && input$backgroundRegionName == 'all')
							myUnionPoly <- rgeos::gUnionCascaded(background)
						else
							myUnionPoly <- rgeos::gUnionCascaded(subset(background[,input$backgroundFieldName],subset=background@data[,input$backgroundFieldName] %in% input$backgroundRegionName))
					}
					else{
						if(!is.null(input$backgroundRegionName) && input$backgroundRegionName == 'all')
							myUnionPoly <- rgeos::gUnaryUnion(background)
						else
							myUnionPoly <- rgeos::gUnaryUnion(subset(background[,input$backgroundFieldName],subset=background@data[,input$backgroundFieldName] %in% input$backgroundRegionName))
					}

					#if(gIsValid(myUnionPoly)){
						background_name <- input$backgroundShape_files #input$polygon_name
						rgdal::writeOGR(SpatialPolygonsDataFrame(myUnionPoly,data.frame(ID=1,AREA=as.numeric(format(raster::area(myUnionPoly),scientific=TRUE)))), backgroundShapeDir(), sprintf("SHBM_%s",background_name), driver="ESRI Shapefile",overwrite_layer = T)
					# }
					# else
						# stop("dissolving failed")
				}

			}
		)
	}
)

# disables "update polygon" button while "run dissolve" button is not pressed
observe({
    toggleState("update_background_layer",
		condition = input$Dissolve_BG > 0
	)
})

# --------------------------------------------------------

# REACTIVE FUNCTIONS

# --------------------------------------------------------

leafMapBg <- eventReactive(input$background_extent_Clip, ignoreNULL=FALSE,{

	# Draw map leaflet map
  map <- leaflet(options = leaflet::leafletOptions(worldCopyJump=T)) %>%

    leaflet::addProviderTiles(leaflet::providers$Esri.OceanBasemap, group="Esri")

	background_extent <- input$background_extent

	# Add draw bar tool
	if(!is.null(background_extent) && background_extent=='TOOL'){

		map <- map %>%

		addMeasure(primaryLengthUnit = "kilometers") %>%

		addDrawToolbar(targetGroup 	= "polyGons",

						position	='topleft',

						singleFeature = TRUE,

						editOptions = editToolbarOptions(

							edit = TRUE,

							selectedPathOptions = selectedPathOptions(color="red", fillColor="red")

						)
		)
	}

	# Add points
	if(!is.null(data_occ_background()) && input$background_extent_Clip >0L && ((!is.null(input$background_extent_add_occ_data) && input$background_extent_add_occ_data==TRUE)) ){

		map <- map %>%

		addCircles(radius = 5000, layerId = data_occ_background()[,4], group="Occ",

			lng = data_occ_background()[,2],

			lat = data_occ_background()[,3],

			#popup = ~paste(ID),

			color = "black",

			fillOpacity = 0.7
		)
	}

	# Add polygon
	if( !is.null(input$background_extent_Clip) && input$background_extent_Clip>0 && !is.null(background_extent) && background_extent!='TOOL' && !is.null(myBackground())){

	  speciesOcc$focal <- input$background_area_sp$right[1]

	  my.background <- myBackground()

		map <- map %>%

		addPolygons(data=my.background, group="Polygon",

					color="red",

					fillColor="red",

					fillOpacity = 0.3
		)

	}

	# Set extent
	if( !is.null(data_occ_background()) && !is.null(input$checkBackgroundMap) &&  !input$checkBackgroundMap){

		map <- map %>%

		setView(lat=median(data_occ_background()[,3]),lng=median(data_occ_background()[,2]), zoom=4, options=list(maxZoom=11))
	}
	else if(!is.null(input$checkBackgroundMap) && input$checkBackgroundMap){
	  map <- map %>%

	    setView(lat=median(sf::st_bbox(my.background)[c("ymin","ymax")]),lng=median(sf::st_bbox(my.background)[c("xmin","xmax")]), zoom=4, options=list(maxZoom=11))
	}
	else{
		map <- map %>%

		setView(lat=0,lng=0, zoom=3, options=list(maxZoom=11))
	}

	if(!is.null(myBackground())){
	  is_plotted_background_extent(TRUE)
	  can_save_background(TRUE)
	  can_delete_background(FALSE)
  }
	return(map)

})


# Returns occurence dataset selected by user
data_occ_background <- reactive({

	if(!is.null(input$occ_data_for_background_extent) && input$occ_data_for_background_extent == "raw_occ"){

		data <- data_input()
		if(is.null(data))
		  return(data)

		data$ID <- 1:nrow(data)

		return(	data[, c('ID', input$User_XLongitude, input$User_YLatitude, input$User_Species_name)] )
	}

	if(!is.null(input$occ_data_for_background_extent) && input$occ_data_for_background_extent == "subset_occ"){

		data <- data_select()
		if(is.null(data))
		  return(data)

		data$ID <- 1:nrow(data)

		return(	data[, c('ID', input$User_XLongitude, input$User_YLatitude, input$User_Species_name)] )
	}

  if(!is.null(input$occ_data_for_background_extent) && input$occ_data_for_background_extent == "filter_occ"){

    list.species.with.filtered.occ <- isolate(reactiveValuesToList(spatial_thinning_analysis_selected_dataset))

    sp.names <- intersect(input$background_area_sp$right, names(list.species.with.filtered.occ))

    if(length(sp.names)==0L) return(NULL)

    nbrows <- sapply(list.species.with.filtered.occ[sp.names], nrow)

    sp.column.names <- NULL

    for(k in 1:length(nbrows))  sp.column.names <- c(sp.column.names, rep(names(list.species.with.filtered.occ[sp.names])[k], times=nbrows[k]))

    data <- do.call("rbind", list.species.with.filtered.occ[sp.names])

    data <- cbind(ID=1:nrow(data), data, sp.column.names)

    colnames(data) <- c('ID', input$User_XLongitude, input$User_YLatitude, input$User_Species_name)

    return(	data )
  }

	if(!is.null(input$occ_data_for_background_extent) && input$occ_data_for_background_extent == "clean_occ"){

		data <- data_clean()
		if(is.null(data))
		  return(data)

		data$ID <- 1:nrow(data)

		return(	data[, c('ID', input$User_XLongitude, input$User_YLatitude, input$User_Species_name)] )
	}

	return(NULL)
})

# returns the polygon layer of the constrained area
myBackground <- reactive({

  if(is.null(input$background_extent_Clip) || input$background_extent_Clip==0) return(NULL)

	map 				        <- NULL

	occ_dat_bg          <- if(is.null(speciesOcc$focal)) isolate(data_occ_background()) else if(speciesOcc$focal %in% isolate(unique(data_occ_background()[,input$User_Species_name]))) isolate(data_occ_background()[data_occ_background()[,input$User_Species_name]==speciesOcc$focal,]) else isolate(data_occ_background())

	background_extent 	<- isolate(input$background_extent)

	# Read polygon from user file
	if(!is.null(background_extent) && background_extent=='POLY' && !is.null(backgroundShapeDir()) && !is.null(input$backgroundShape_files) && input$backgroundShape_files %in% backgroundShapelayers()){
		# Read polygon from temporary file
		fn <- file.path(dirname(rasterTmpFile()),
		             paste0('myPolygon_shp',gsub("\\(([^()]+)\\)","",speciesOcc$focal),paste(colnames(occ_dat_bg),collapse=''),
		                    nrow(occ_dat_bg),ncol(occ_dat_bg),
		                    switch(input$clip_background_extent,COAST='x',LAND='y',NO='z'),'.rds'))
		if(file.exists(fn)){
			map <- readRDS(fn)
			return(map)
		}
		map 		<- sf::st_read(file.path(backgroundShapeDir(),paste0(isolate(input$backgroundShape_files),".shp")), quiet=TRUE) %>%
		  sf::st_make_valid() %>%
		  lwgeom::st_transform_proj(crs="+proj=wintri")
	}
	else if(!is.null(background_extent) && background_extent=='BBOX'){
		# Read polygon from temporary file
		fn <- file.path(dirname(rasterTmpFile()),
		             paste0('myPolygon_bbx',gsub("\\(([^()]+)\\)","",speciesOcc$focal),paste(colnames(occ_dat_bg),collapse=''),
		                    nrow(occ_dat_bg),ncol(occ_dat_bg),
		                    switch(input$clip_background_extent,COAST='x',LAND='y',NO='z'),'.rds'))
		if(file.exists(fn)){
			map <- readRDS(fn)
			return(map)
		}
		if(is.null(occ_dat_bg)) return(map)
		n = max(occ_dat_bg[,3]); s = min(occ_dat_bg[,3]); w = min(occ_dat_bg[,2]); e = max(occ_dat_bg[,2])
		map0 <- sf::st_as_sf(sf::st_as_sfc(sf::st_bbox(c(xmin=w, xmax=e, ymin=s, ymax=n), crs=sf::st_crs(4326))))#rgeos::bbox2SP(n,s,w,e)
		rad  <- isolate(input$background_extent_buffer_size)*1000
		map1 <- lwgeom::st_transform_proj(map0, crs="+proj=wintri")
		map  <- sf::st_make_valid(sf::st_buffer(map1, dist=rad))
	}
	else if(!is.null(background_extent) && background_extent=='HULL'){
		# Read polygon from temporary file
		fn <- file.path(dirname(rasterTmpFile()),
		             paste0('myPolygon_hull',gsub("\\(([^()]+)\\)","",speciesOcc$focal),paste(colnames(occ_dat_bg),collapse=''),
		                    nrow(occ_dat_bg),ncol(occ_dat_bg),
		                    switch(input$clip_background_extent,COAST='x',LAND='y',NO='z'),'.rds'))
		if(file.exists(fn)){
			map <- readRDS(fn)
			return(map)
		}
		if(is.null(occ_dat_bg)) return(map)
		htps0 	<- sf::st_as_sf(occ_dat_bg, coords=2:3, crs=sf::st_crs(4326))
		htps    <- lwgeom::st_transform_proj(htps0, crs="+proj=wintri")
		map0 		<- sf::st_as_sf(sf::st_convex_hull(sf::st_union(htps)))
		rad     <- isolate(input$background_extent_buffer_size)*1000
		map     <- sf::st_make_valid(sf::st_buffer(map0, dist=rad))
	}
	else if(!is.null(background_extent) && background_extent=='BUFFER'){
		# Read polygon from temporary file
		fn <- file.path(dirname(rasterTmpFile()),
		             paste0('myPolygon_circle',gsub("\\(([^()]+)\\)","",speciesOcc$focal),paste(colnames(occ_dat_bg),collapse=''),
		                    nrow(occ_dat_bg),ncol(occ_dat_bg),
		                    input$background_extent_buffer_size*1000,
		                    switch(input$clip_background_extent,COAST='x',LAND='y',NO='z'),'.rds'))
		if(file.exists(fn)){
			map <- readRDS(fn)
			return(map)
		}
		if(is.null(occ_dat_bg)) return(map)
		polXY0	<- sf::st_as_sf(occ_dat_bg, coords=2:3, crs=sf::st_crs(4326))
		polXY   <- lwgeom::st_transform_proj(polXY0, crs="+proj=wintri")
		rad     <- isolate(input$background_extent_buffer_size)*1000
		map		<- sf::st_as_sf(sf::st_union(sf::st_buffer(polXY, dist=rad)))
	}

  if(!is.null(map) && !is.null(isolate(input$clip_background_extent))){
    tmp = map
    # keep terrestrial lands intersecting with the training background
    land_tmp <- try(apply(sf::st_intersects(shbmd_geodata$terrestrial_lands, tmp, sparse=FALSE),1,any),silent=TRUE)

    if(inherits(land_tmp,"try-error")){
      map0 <- tmp
    }else if(sum(land_tmp>0L)){
      suppressWarnings({
        map0 = switch(input$clip_background_extent,

               "COAST"= tryCatch({
                 land_cropped <- sf::st_crop(shbmd_geodata$terrestrial_lands[land_tmp,],tmp)
                 sf::st_intersection(tmp, land_cropped)
                 },
                 error=function(err) return(tmp)),

               "LAND" = tryCatch({
                 land_cropped <- sf::st_crop(shbmd_geodata$terrestrial_lands[land_tmp,],tmp)
                 sf::st_difference(tmp, sf::st_combine(land_cropped))
                 },
                 error=function(err) return(tmp)),

               "NO" = tmp
          )
      })
    }else{
      map0 <- tmp
    }
    map <- lwgeom::st_transform_proj(map0, crs=sf::st_crs(4326))
  }

	# save in temporary file
	if(!is.null(map)) saveRDS(map, file=fn)

	return(map)
})

# returns list of field name of polygon attribute table
search_backgroundregion <-reactive({
	if (!is.null(lengthBackground()) && lengthBackground()>1 && !is.null(input$backgroundFieldName))
		levels(sf::st_drop_geometry(myBackground())[,input$backgroundFieldName])
	else
		return(NULL)
})

# returns generated polygon name
search_background_layer<- reactive({
	if(!is.null(backgroundShapelayers()) && sprintf("SHBM_%s",input$backgroundShape_files) %in% backgroundShapelayers()){
		return(sprintf("SHBM_%s",input$backgroundShape_files))
	}
	else
		return(NULL)
})

# returns the number of subgeometries of the polygon layer
lengthBackground <-reactive({
	if(!is.null(input$backgroundShape_files) && !is.null(myBackground())){
		length(sf::st_geometry(myBackground()))
	}
	else
		return(NULL)
})

# reads and returns polygon layer directory
backgroundShapeDir <- reactive({

	path <- readDirectoryInput(session, 'backgroundshape_directory')

	if(!is.null(path) && nchar(path)>0L)
		return(path)
	else
		return(NULL)
})

# returns list of shapefile layers within region layer directory
backgroundShapelayers <- reactive({

	if(!is.null(backgroundShapeDir())){

		listLayers = tryCatch(rgdal::ogrListLayers(dsn=backgroundShapeDir()), warning=function(warn) return(NULL), error = function(err) return(NULL))

		return(listLayers)
	}
	else
		return(NULL)
})

# --------------------------------------------------------

# OUTPUTS

# --------------------------------------------------------
# displays a chooser box of species available for building (i.e. with formated data in the biomod formated directory)
output$chooserConstrainedAreaSpecies  <-renderUI({

  hasFormatedData()
  input$background_extent

  tagList(

    strong(p("Select your species")),

    chooserInput(inputId="background_area_sp",

                 leftLabel="Available species",

                 rightLabel="Selected species",

                 leftChoices= c(),

                 rightChoices = if(is.null(data_occ_background())) "" else unique(data_occ_background()[,4]),

                 size = if(is.null(data_occ_background())) 0 else min(6,length(unique(data_occ_background()[,4]))),

                 multiple=TRUE
    )
  )
})
outputOptions(output,"chooserConstrainedAreaSpecies", suspendWhenHidden=FALSE)

output$BgMap <- renderLeaflet({

  return(leafMapBg())

})

output$polygonInfo <- shiny::renderUI({

  if(is.null(input$background_extent_Clip)|| input$background_extent_Clip==0L || is.null(input$background_area_sp$right) || length(input$background_area_sp$right)==0L)
    return()

  input$background_area_sp$right
  current_background()

  isolate({

      list.background <- list.files(dirname(rasterTmpFile()), pattern=paste0("^myPolygon_.+\\.rds$"), full.names=TRUE)
      list.species.background <- unlist(sapply(gsub("\\(([^()]+)\\)","(.*?)",input$background_area_sp$right), grep, x=list.background,value=TRUE,USE.NAMES=FALSE))

      if(length(list.species.background)==0L) return()

      species_names = str_match(basename(list.species.background), pattern=paste0("(?<=hull|bbx|shp|circle)(.*?)",paste(colnames(data_occ_background()),collapse='')))[,2]
      species_name = species_names[current_background()]

      species.background.names = str_match(basename(list.species.background), pattern=paste0("^myPolygon",'.(.*?)', species_name))[,2]
      species.background.name = species.background.names[current_background()]

      species.background = try(readRDS(list.species.background[current_background()]),silent=TRUE)

      if(inherits(species.background,"try-error"))
        return()

      if(!is.null(data_occ_background())){
        sp.pts0 <- sf::st_as_sf(subset(data_occ_background(), subset=grepl(species_name, data_occ_background()[,4]), select=2:3),
                               coords=1:2,
                               crs=sf::st_crs(species.background))
        sp.pts <- lwgeom::st_transform_proj(sp.pts0, crs="+proj=wintri")
        species.background.wintri <- lwgeom::st_transform_proj(species.background, crs="+proj=wintri")

        outpts  <- sum(rowSums(sf::st_intersects(sp.pts, species.background.wintri, sparse=FALSE))==0L) #sum(!complete.cases(sp::over(sp.pts, species.background)))
        prc.pts <- round((outpts/length(sf::st_geometry(sp.pts))) * 100, 2)
        # if(prc.pts==100)
        #   can_save_background(FALSE)

      }else{
        prc.pts <- NULL
      }

      messages <- sprintf(
        "<span style='font-size: 22px;'>%s</span><br><br>
    					                     <p>
    					                     <strong>Constraint method:&nbsp;</strong><span class='label label-primary'>%s </span><br>
    					                     <strong>Approximative area of the constrained region:&nbsp;</strong> %g km2<br>
    					                     <strong>Number of occurrence records left outside:&nbsp;</strong>%s<br>
    					                     %s
    					                     </p>",
        paste(species_name,"<span class='label undefined'>Constrained training area</span>",format(Sys.time(),'%d/%m/%Y %H:%M:%S'),sep=" - "),
        switch(species.background.name,"tool"="Drawn on Map","shp"="Pre-defined region","bbx"="Bounding Box","hull"="Convex Hull",
               "envprof"="Environmnental Profiling","circle"="Circular Buffers"),
        sum(sf::st_area(species.background.wintri))/1e+06,
        if(is.null(prc.pts)) "Not available" else if(prc.pts<=5) paste(outpts,"(",paste0("<span class='label label-success'>",prc.pts," %</span>"),")") else if(prc.pts<50) paste(outpts,"(",paste0("<span class='label label-warning'>",prc.pts," %</span>"),")") else paste0(outpts,"(",paste0("<span class='label label-danger'>",prc.pts," %</span>"),")"),
        if(!is.null(prc.pts) && prc.pts==100) "<br><div class='warning' style='text-align: justify;'><p style='padding-left: 1em; padding-top: 1em; padding-bottom: 1em; color:#8d5524'><strong><i class='fas fa-exclamation-triangle'></i> No occurrence records are falling inside this background !! You may wish to select another option to constrain your training area.</p></div><br>" else ""
      )

      tagList(

        tags$hr(style="border: 2px solid !important; color: #fcc201 !important;"),

        fluidRow(style="font-family: times, monospace; font-size: 18px;",
                 column(12,
                        column(8,
                               HTML(gsub("\\n","",messages[messages!='']))
                        ),
                        column(2,
                               br(),
                               withBusyIndicatorUI(bsButton(inputId="save_background_extent_Clip", style="success", icon=icon('inbox', lib='font-awesome'), label='save')),
                               withBusyIndicatorUI(bsButton(inputId="delete_background_extent_Clip", style="danger", icon=icon('trash-alt', lib='font-awesome'), label='delete', disabled=!can_delete_background()))
                        ),
                        column(2,
                               div(style="display:inline-block; vertical-align: top; float: right",
                                   bsButton(inputId="background_extent_box_move_right", icon = icon("angle-right","fa-2x"), label = NULL, size="small", disabled=current_background()==length(list.species.background))
                               ),
                               div(style="display:inline-bloc; vertical-align: top; float: right",HTML("<br>")),
                               div(style="display:inline-block; vertical-align: top; float: right",
                                   bsButton(inputId="background_extent_box_move_left",icon = icon("angle-left","fa-2x"), label = NULL, size="small", disabled = current_background()==1)
                               )
                        )
                 )
        ),
        tags$hr(style="border: 2px solid !important; color: #fcc201 !important;")

      )
  })

})

# displays user interface with options to dissolve polygon layer
output$backgroundFeatures <-renderUI({

	if(is.null(lengthBackground()))
		return()

	if(lengthPolygon()>1){

		tagList(

			useShinyjs(),

			h4("Sub-geometry"),

			wellPanel(

				p("Your polygon contains sub-geometries."),

				p("Select the field related to these sub-geometries :"),

				selectizeInput(inputId="backgroundFieldName",

					label=NULL,

					choices=names(myBackground()),

					options=list(placeholder="Select a field")
				),

				p("Select which (sub)region(s) you want to use :"),

				selectizeInput(inputId="backgroundRegionName",

					label=NULL,

					choices=c('All'='all',search_backgroundregion()),

					multiple=TRUE,

					options=list(placeholder="Select a region")
				),

				p("Run",strong('Dissolve'),"to unify them together"),

				withBusyIndicatorUI(

					actionButton(inputId = "Dissolve_BG",

						label="Dissolve",

						class = "btn-primary"
					)
				),

				busyIndicator(ifelse(is.null(isolate(input$Dissolve_BG))|| isolate(input$Dissolve_BG)==0, "Loading...","Dissolving subgeometries..."),wait = 2000),

				br(),

				tags$i("Note: The polygon shape file will be automatically saved in the same directory as input polygon"),

				br(),br(),

				conditionalPanel(

					condition="input.Dissolve_BG > 0",

					actionButton(inputId = 'update_background_layer',

						label='Update background',

						styleclass = "primary"
					)
				)
			)
		)
	}
})

